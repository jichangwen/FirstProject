package com.job.app.define;

import java.util.Map;

/*
 * 
 * 本类用来在前台和后台之间传输数据,后台往前台发送的数据需要封装成该对象再传递
 * 	
 * */
public class MSG {
	private Boolean status;
	private String msg; //提示语
	private Map<String, Object> map;
	
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Map<String, Object> getMap() {
		return map;
	}

	public void setMap(Map<String, Object> map) {
		this.map = map;
	}
}
