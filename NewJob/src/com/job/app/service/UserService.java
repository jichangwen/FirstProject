package com.job.app.service;

import com.job.app.BaseDao;
import com.job.app.entitys.Permission;
import com.job.app.entitys.User;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Objects;

@Service
public class UserService {
    @Autowired
    private BaseDao baseDao;
    public void save(User user) throws Exception {
        System.out.println("service: " + user);
        if (Objects.isNull(user)) {
            throw new Exception("用户信息为空");
        }
        if (StringUtils.isEmpty(user.getEmail())) {
            throw new Exception("邮箱不能为空");
        }
        if (StringUtils.isEmpty(user.getName())) {
            throw new Exception("姓名不能为空");
        }
        if (StringUtils.isEmpty(user.getPassword())) {
            throw new Exception("密码不能为空");
        }
        String hql = "from Permission where permissionLevel = :permissionLevel";
        Permission permission = (Permission) baseDao.getSession().createQuery(hql).setInteger("permissionLevel", 5).uniqueResult();
        if (Objects.isNull(permission)) {
            throw new Exception("该权限不存在");
        }
        user.setPemissonId(permission.getId());
        user.setRegisterTime(new Date());
        user.setGender(0);

        baseDao.getSession().save(user);
    }
}
