package com.job.app.utils;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
final public class HibernateUtil {
	private static SessionFactory sessionFactory=null;
	//使用线程局部模式
	private static ThreadLocal<Session> threadLocal=new ThreadLocal<Session>();
	private HibernateUtil(){};
	static {
		sessionFactory=new Configuration().configure().buildSessionFactory();
	}
	
	//获取全新的全新的sesession
	public static Session openSession(){
		return sessionFactory.openSession();
	}
	//获取和线程关联的session
	public static Session getCurrentSession(){
		
		Session session=threadLocal.get();
		//判断是否得到
		if(session==null){
			session=sessionFactory.openSession();
			//把session对象设置到 threadLocal,相当于该session已经和线程绑定
			threadLocal.set(session);
		}
		return session;	
	}
	//提供一个统一的查询方法通过传入的HQL语句和参数来执行查询操作
	public static List executeQuery(String hql,String[] parameters){
		Session session= null;
		List list = null;
		try {
			session  = openSession();
			Query query = session.createQuery(hql);
			if(parameters != null && parameters.length > 0){
				//参数绑定
				for (int i = 0; i < parameters.length; i++) {
					query.setString(i, parameters[i]);
				}
			}
			list = query.list();	
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}finally{
			if(session != null && session.isOpen()){
				session.close();
			}
		}
		return list;
	}
	
	//统一的插入方法
	public static void save(Object obj){
		Session session = null;
		Transaction tx = null;
		try {
			session = getCurrentSession();
			tx = session.beginTransaction();
			session.save(obj);
			tx.commit();
		} catch (Exception e) {
			if(tx != null){
				tx.rollback();
			}
			throw new RuntimeException(e.getMessage());
		}finally{
			if(session != null && session.isOpen()){
				session.close();
			}
		}
	}
	//提供一个统一的查询方法通过传入的HQL语句和参数来执行查询操作(带分页)
		public static List executeQueryByPage(String hql,String[] parameters,int pageNow,int pageSize){
			Session session= null;
			List list = null;
			try {
				session  = openSession();
				Query query = session.createQuery(hql);
				if(parameters != null && parameters.length > 0){
					//参数绑定
					for (int i = 0; i < parameters.length; i++) {
						query.setString(i, parameters[i]);
					}
				}
				query.setFirstResult((pageNow - 1)*pageSize).setMaxResults(pageSize);
				list = query.list();	
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException(e.getMessage());
			}finally{
				if(session != null && session.isOpen()){
					session.close();
				}
			}
			return list;
		}
		//提供一个统一更新删除
		public static void executeUpdate(String hql,String[] parameters){
			Session session= null;
			Transaction tx = null;
			try {
				session  = openSession();
				tx = session.beginTransaction();
				Query query = session.createQuery(hql);
				if(parameters != null && parameters.length > 0){
					//参数绑定
					for (int i = 0; i < parameters.length; i++) {
						query.setString(i, parameters[i]);
					}
				}
				query.executeUpdate(); //调用此方法来更新操作
				tx.commit();
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException(e.getMessage());
			}finally{
				if(session != null && session.isOpen()){
					session.close();
				}
			}
		}
}
