package com.job.app.Action;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import com.job.app.define.MSG;
import com.job.app.entitys.User;
import com.job.app.service.UserService;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class UserAction extends ActionSupport implements ModelDriven<User> {

	private static final long serialVersionUID = 1L;

	private User user = new User();
	@Autowired
	private UserService userService;
	private String result;

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String add() {
		System.out.println("HelloWorld");
		return "success";
	}

	public String main() {
		return "main";
	}

	@JSONField(serialize = false)
	private MSG msg;

	public MSG getMsg() {
		return msg;
	}

	public void setMsg(MSG msg) {
		this.msg = msg;
	}

	/**
	 * 用户注册: 返回 json
	 * @return
	 */
	public String register() {
		System.out.println("--->: "+ user);
		msg = new MSG();
		try {
			userService.save(user);
			msg.setStatus(true);
			msg.setMsg("注册成功");
		} catch (Exception e) {
			msg.setStatus(false);
			msg.setMsg("注册失败");
			e.printStackTrace();
		}
		result = JSON.toJSONString(msg);
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setCharacterEncoding("UTF-8");
			response.setContentType("text/*;charset=utf-8");
			response.getWriter().write(result);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "none";
	}

	@Override
	public User getModel() {
		return user;
	}
}
