package com.job.app.entitys;

import java.sql.Date;
import java.util.Objects;

public class Administrator {
    private int id;
    private String adminName;
    private String position;
    private Integer age;
    private Date createTime;
    private int permissionId;
    private String remark;
    private String temp1;
    private String temp2;
    private String temp3;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public int getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(int permissionId) {
        this.permissionId = permissionId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getTemp1() {
        return temp1;
    }

    public void setTemp1(String temp1) {
        this.temp1 = temp1;
    }

    public String getTemp2() {
        return temp2;
    }

    public void setTemp2(String temp2) {
        this.temp2 = temp2;
    }

    public String getTemp3() {
        return temp3;
    }

    public void setTemp3(String temp3) {
        this.temp3 = temp3;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Administrator that = (Administrator) o;
        return id == that.id &&
                permissionId == that.permissionId &&
                Objects.equals(adminName, that.adminName) &&
                Objects.equals(position, that.position) &&
                Objects.equals(age, that.age) &&
                Objects.equals(createTime, that.createTime) &&
                Objects.equals(remark, that.remark) &&
                Objects.equals(temp1, that.temp1) &&
                Objects.equals(temp2, that.temp2) &&
                Objects.equals(temp3, that.temp3);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, adminName, position, age, createTime, permissionId, remark, temp1, temp2, temp3);
    }

    public Administrator() {
    }

    public Administrator(int id, String adminName, String position, Integer age, Date createTime, int permissionId, String remark, String temp1, String temp2, String temp3) {
        this.id = id;
        this.adminName = adminName;
        this.position = position;
        this.age = age;
        this.createTime = createTime;
        this.permissionId = permissionId;
        this.remark = remark;
        this.temp1 = temp1;
        this.temp2 = temp2;
        this.temp3 = temp3;
    }

    @Override
    public String toString() {
        return "Administrator{" +
                "id=" + id +
                ", adminName='" + adminName + '\'' +
                ", position='" + position + '\'' +
                ", age=" + age +
                ", createTime=" + createTime +
                ", permissionId=" + permissionId +
                ", remark='" + remark + '\'' +
                ", temp1='" + temp1 + '\'' +
                ", temp2='" + temp2 + '\'' +
                ", temp3='" + temp3 + '\'' +
                '}';
    }
}
