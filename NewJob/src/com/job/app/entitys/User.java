package com.job.app.entitys;

import java.util.Date;
import java.util.Objects;

public class User {
    private int id;
    private String name;
    private Integer gender;
    private Integer age;
    private String email;
    private String phone;
    private Date born;
    private String bornSheng;
    private String bornShi;
    private String imgUrl;
    private Date attendWorkDate;
    private String nowSheng;
    private String nowShi;
    private String nowQu;
    private Integer isWorkExperience;
    private String password;
    private Date registerTime;
    private int pemissonId;
    private String remark;
    private String temp1;
    private String temp2;
    private String temp3;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getBorn() {
        return born;
    }

    public void setBorn(Date born) {
        this.born = born;
    }

    public String getBornSheng() {
        return bornSheng;
    }

    public void setBornSheng(String bornSheng) {
        this.bornSheng = bornSheng;
    }

    public String getBornShi() {
        return bornShi;
    }

    public void setBornShi(String bornShi) {
        this.bornShi = bornShi;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Date getAttendWorkDate() {
        return attendWorkDate;
    }

    public void setAttendWorkDate(Date attendWorkDate) {
        this.attendWorkDate = attendWorkDate;
    }

    public String getNowSheng() {
        return nowSheng;
    }

    public void setNowSheng(String nowSheng) {
        this.nowSheng = nowSheng;
    }

    public String getNowShi() {
        return nowShi;
    }

    public void setNowShi(String nowShi) {
        this.nowShi = nowShi;
    }

    public String getNowQu() {
        return nowQu;
    }

    public void setNowQu(String nowQu) {
        this.nowQu = nowQu;
    }

    public Integer getIsWorkExperience() {
        return isWorkExperience;
    }

    public void setIsWorkExperience(Integer isWorkExperience) {
        this.isWorkExperience = isWorkExperience;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(Date registerTime) {
        this.registerTime = registerTime;
    }

    public int getPemissonId() {
        return pemissonId;
    }

    public void setPemissonId(int pemissonId) {
        this.pemissonId = pemissonId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getTemp1() {
        return temp1;
    }

    public void setTemp1(String temp1) {
        this.temp1 = temp1;
    }

    public String getTemp2() {
        return temp2;
    }

    public void setTemp2(String temp2) {
        this.temp2 = temp2;
    }

    public String getTemp3() {
        return temp3;
    }

    public void setTemp3(String temp3) {
        this.temp3 = temp3;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                pemissonId == user.pemissonId &&
                Objects.equals(name, user.name) &&
                Objects.equals(gender, user.gender) &&
                Objects.equals(age, user.age) &&
                Objects.equals(email, user.email) &&
                Objects.equals(phone, user.phone) &&
                Objects.equals(born, user.born) &&
                Objects.equals(bornSheng, user.bornSheng) &&
                Objects.equals(bornShi, user.bornShi) &&
                Objects.equals(imgUrl, user.imgUrl) &&
                Objects.equals(attendWorkDate, user.attendWorkDate) &&
                Objects.equals(nowSheng, user.nowSheng) &&
                Objects.equals(nowShi, user.nowShi) &&
                Objects.equals(nowQu, user.nowQu) &&
                Objects.equals(isWorkExperience, user.isWorkExperience) &&
                Objects.equals(password, user.password) &&
                Objects.equals(registerTime, user.registerTime) &&
                Objects.equals(remark, user.remark) &&
                Objects.equals(temp1, user.temp1) &&
                Objects.equals(temp2, user.temp2) &&
                Objects.equals(temp3, user.temp3);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, gender, age, email, phone, born, bornSheng, bornShi, imgUrl, attendWorkDate, nowSheng, nowShi, nowQu, isWorkExperience, password, registerTime, pemissonId, remark, temp1, temp2, temp3);
    }

    public User(int id, String name, Integer gender, Integer age, String email, String phone, Date born, String bornSheng, String bornShi, String imgUrl, Date attendWorkDate, String nowSheng, String nowShi, String nowQu, Integer isWorkExperience, String password, Date registerTime, int pemissonId, String remark, String temp1, String temp2, String temp3) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.email = email;
        this.phone = phone;
        this.born = born;
        this.bornSheng = bornSheng;
        this.bornShi = bornShi;
        this.imgUrl = imgUrl;
        this.attendWorkDate = attendWorkDate;
        this.nowSheng = nowSheng;
        this.nowShi = nowShi;
        this.nowQu = nowQu;
        this.isWorkExperience = isWorkExperience;
        this.password = password;
        this.registerTime = registerTime;
        this.pemissonId = pemissonId;
        this.remark = remark;
        this.temp1 = temp1;
        this.temp2 = temp2;
        this.temp3 = temp3;
    }

    public User() {
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", gender=" + gender +
                ", age=" + age +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", born=" + born +
                ", bornSheng='" + bornSheng + '\'' +
                ", bornShi='" + bornShi + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", attendWorkDate=" + attendWorkDate +
                ", nowSheng='" + nowSheng + '\'' +
                ", nowShi='" + nowShi + '\'' +
                ", nowQu='" + nowQu + '\'' +
                ", isWorkExperience=" + isWorkExperience +
                ", password='" + password + '\'' +
                ", registerTime=" + registerTime +
                ", pemissonId=" + pemissonId +
                ", remark='" + remark + '\'' +
                ", temp1='" + temp1 + '\'' +
                ", temp2='" + temp2 + '\'' +
                ", temp3='" + temp3 + '\'' +
                '}';
    }
}
