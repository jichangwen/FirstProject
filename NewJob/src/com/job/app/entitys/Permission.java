package com.job.app.entitys;

import java.sql.Date;
import java.util.Objects;

public class Permission {
    private int id;
    private String permissionName;
    private int permissionLevel;
    private Date createDate;
    private String remark;
    private String temp1;
    private String temp2;
    private String temp3;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    public int getPermissionLevel() {
        return permissionLevel;
    }

    public void setPermissionLevel(int permissionLevel) {
        this.permissionLevel = permissionLevel;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getTemp1() {
        return temp1;
    }

    public void setTemp1(String temp1) {
        this.temp1 = temp1;
    }

    public String getTemp2() {
        return temp2;
    }

    public void setTemp2(String temp2) {
        this.temp2 = temp2;
    }

    public String getTemp3() {
        return temp3;
    }

    public void setTemp3(String temp3) {
        this.temp3 = temp3;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Permission that = (Permission) o;
        return id == that.id &&
                permissionLevel == that.permissionLevel &&
                Objects.equals(permissionName, that.permissionName) &&
                Objects.equals(createDate, that.createDate) &&
                Objects.equals(remark, that.remark) &&
                Objects.equals(temp1, that.temp1) &&
                Objects.equals(temp2, that.temp2) &&
                Objects.equals(temp3, that.temp3);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, permissionName, permissionLevel, createDate, remark, temp1, temp2, temp3);
    }

    public Permission() {
    }

    public Permission(int id, String permissionName, int permissionLevel, Date createDate, String remark, String temp1, String temp2, String temp3) {
        this.id = id;
        this.permissionName = permissionName;
        this.permissionLevel = permissionLevel;
        this.createDate = createDate;
        this.remark = remark;
        this.temp1 = temp1;
        this.temp2 = temp2;
        this.temp3 = temp3;
    }

    @Override
    public String toString() {
        return "Permission{" +
                "id=" + id +
                ", permissionName='" + permissionName + '\'' +
                ", permissionLevel=" + permissionLevel +
                ", createDate=" + createDate +
                ", remark='" + remark + '\'' +
                ", temp1='" + temp1 + '\'' +
                ", temp2='" + temp2 + '\'' +
                ", temp3='" + temp3 + '\'' +
                '}';
    }
}
