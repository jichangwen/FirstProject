<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'register.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/staticResource/beforeResource/style/css/style.css"/>
	<script src="${pageContext.request.contextPath}/staticResource/beforeResource/style/js/jquery.1.10.1.min.js" type="text/javascript"></script>	
	<script type="text/javascript" src="${pageContext.request.contextPath}/staticResource/beforeResource/style/js/jquery.lib.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/staticResource/beforeResource/style/js/core.min.js"></script>
	

  </head>
  
 <body id="login_bg">
	<div class="login_wrapper">
		<div class="login_header">
        	<a href="h/"><h1 width="285" height="62">I Want To NewJob</h1></a>
        </div>
        
    	 
		<div class="login_box">
			<div class="header">
				<div class="switch" id="switch">
					<a class="switch_btn_focus" id="switch_qlogin" href="javascript:void(0);" tabindex="7">邮箱注册</a>
					<div class="switch_bottom" id="switch_bottom" style="position: absolute; width: 64px; left: 0px;"></div>
				</div>
			</div> 
			<!--邮箱注册-->
			<div id="emailDiv">
				<form id="loginForm" style="display:block;">
					
					<input type="text" id="email" name="email" tabindex="1" placeholder="请输入常用邮箱地址" />
					<span class="error" style="display:none;" id="beError"></span>
					<input type="password" id="password" name="password" tabindex="2" placeholder="请输入密码" />
					<label class="fl registerJianJu" for="checkbox">
						<input type="checkbox" id="checkbox" name="checkbox" checked  class="checkbox valid" />我已阅读并同意<a href="h/privacy.html" target="_blank">《NewJob用户协议》</a>
					</label>
					<input type="submit" id="submitLogin" value="注 &nbsp; &nbsp; 册" />
				</form>
			</div>
			<!--邮箱注册结束-->
			
			
            <div class="login_right">
            	<div>已有NewJob帐号</div>
            	<a  href="${pageContext.request.contextPath}/view/public/login.jsp"  class="registor_now">直接登录</a>
                <div class="login_others">使用以下帐号直接登录:</div>
                <a  href="h/ologin/auth/sina.html"  target="_blank" class="icon_wb" title="使用新浪微博帐号登录"></a>
               	<a  href="h/ologin/auth/qq.html"  class="icon_qq" target="_blank" title="使用腾讯QQ帐号登录" ></a>
            </div>
        </div>
        <div class="login_box_btm"></div>
    </div>
  
</body>
</html>
