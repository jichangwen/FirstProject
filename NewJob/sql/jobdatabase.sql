﻿/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : mysql
 Source Server Version : 100132
 Source Host           : localhost:3306
 Source Schema         : jobdatabase

 Target Server Type    : mysql
 Target Server Version : 100132
 File Encoding         : 65001

 Date: 22/12/2018 15:51:53
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for administrator
-- ----------------------------
DROP TABLE IF EXISTS `administrator`;
CREATE TABLE `administrator`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adminName` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '管理员名称',
  `position` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '管理员职位',
  `age` int(11) DEFAULT NULL COMMENT '管理员年龄',
  `createTime` date NOT NULL COMMENT '创建时间',
  `permissionId` int(11) NOT NULL,
  `remark` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '备注',
  `temp1` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `temp2` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `temp3` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `permission`(`permissionId`) USING BTREE,
  CONSTRAINT `permission` FOREIGN KEY (`permissionId`) REFERENCES `permission` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of administrator
-- ----------------------------
INSERT INTO `administrator` VALUES (1, 'admin', '管理员1', 23, '2018-12-18', 1, '无', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for permission
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissionName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限名称',
  `permissionLevel` int(11) NOT NULL COMMENT '权限等级',
  `createDate` date NOT NULL COMMENT '权限项创建时间',
  `remark` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '备注',
  `temp1` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `temp2` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `temp3` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of permission
-- ----------------------------
INSERT INTO `permission` VALUES (1, '普通用户', 5, '2018-12-18', '无', NULL, NULL, NULL);
INSERT INTO `permission` VALUES (2, '会员', 20, '2018-12-18', '无', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '姓名',
  `gender` bit(1) NOT NULL DEFAULT b'1' COMMENT '性别',
  `age` int(11) DEFAULT NULL COMMENT '年龄',
  `email` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'email',
  `phone` char(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '电话',
  `born` date DEFAULT NULL COMMENT '生日',
  `bornSheng` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '籍贯省',
  `bornShi` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '籍贯市',
  `imgUrl` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '图片url',
  `attendWorkDate` date DEFAULT NULL COMMENT '参加工作时间',
  `nowSheng` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '当前居住的省',
  `nowShi` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '当前居住的市',
  `nowQu` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '当前居住的区',
  `isWorkExperience` bit(1) DEFAULT b'1' COMMENT '是否有工作经历',
  `password` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '登录密码',
  `registerTime` date DEFAULT NULL COMMENT '账号注册时间',
  `pemissonId` int(11) NOT NULL DEFAULT 1 COMMENT '权限项id',
  `remark` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `temp1` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `temp2` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `temp3` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `pemission`(`pemissonId`) USING BTREE,
  CONSTRAINT `pemission` FOREIGN KEY (`pemissonId`) REFERENCES `permission` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, '张三', b'1', 24, '1347423680@qq.com', '18791986604', '2018-12-18', '陕西省', '商洛市', NULL, '2018-12-18', '陕西省', '西安市', '碑林区', b'1', '123456', '2018-12-18', 1, NULL, NULL, NULL, NULL);
INSERT INTO `user` VALUES (2, '1571449371@qq.com', b'0', NULL, '1571449371@qq.com', '18392136187', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'hhggjh', '2018-12-22', 1, NULL, NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
