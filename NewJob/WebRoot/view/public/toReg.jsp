<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <base href="<%=basePath%>">
    <title>New Job</title>

    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/staticResource/beforeResource/style/css/style.css"/>
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/staticResource/bootstrap/css/bootstrap.min.css"/>
    <script src="${pageContext.request.contextPath}/staticResource/js/jquery-2.2.3.min.js"
            type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/staticResource/bootstrap/js/bootstrap.min.js"
            type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/staticResource/js/bootbox.js" type="text/javascript"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/staticResource/beforeResource/style/js/jquery.lib.min.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/staticResource/beforeResource/style/js/core.min.js"></script>
</head>
<body>

<div class="content_box cleafix">
    <div class="app_download_layer"></div>
    <div class="register_type worker fl">
        <h3>找工作</h3>
        <div class="worker_image"></div>
        <div class="btn_group"><a href="${pageContext.request.contextPath}/view/public/register.jsp">去找工作</a></div>
    </div>
    <div class="divider fl"></div>
    <div class="register_type hr fl">
        <h3>招人</h3>
        <div class="hr_image"></div>
        <div class="btn_group"><a href="">去发布职位</a></div>
    </div>
</div>

</body>
</html>
